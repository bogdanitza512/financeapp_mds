import 'package:flutter/material.dart';
import 'package:financeapp_mds/shared/constants.dart';
import 'package:financeapp_mds/models/user.dart';
import 'package:provider/provider.dart';
import 'package:financeapp_mds/services/database.dart';
import 'package:financeapp_mds/shared/loading.dart';


class FinancialFormula extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: Text('Financial Formulas'),
          backgroundColor: HexColor("#4A69FF"),

        ),
      resizeToAvoidBottomPadding: false,
      backgroundColor: Colors.white,
      body: Center(
        child: Container(
          decoration: BoxDecoration(
            boxShadow: [
              new BoxShadow(
                color: Colors.grey[300],
                offset: new Offset(0, 0),
                blurRadius: 15,
                spreadRadius: 10
              )
            ],
            color: Colors.white,
            borderRadius: BorderRadius.all(Radius.circular(10))
          ),
          width: 9 * MediaQuery.of(context).size.width / 10,
          height: 2.0 * MediaQuery.of(context).size.height / 3,
          padding: EdgeInsets.fromLTRB(30, 24, 30, 0),
          child: Column(
            children: <Widget>[
              Text(
                "Your Budget",
                style: TextStyle(fontSize: 16),
                
              ),
              SizedBox(height:10),
              BudgetInput(),
              SizedBox(height:30),
              Container(
                child: Center(
                  child: Text(
                    "     You have 3 categories you split your budget into(in this order):\n" + 
                    "   1.Regular expenses - these include your monthly expenses on your bills, food, car gas, etc.\n" +
                    "   2.Investment - you can either save money or choose to invest it into something(ex: a business)\n"+
                    "   3.Personal expenses - with this amount you can buy whatever you feel like buying for yourself",
                    style: TextStyle(
                      fontSize: 14,
                      color: Colors.white,
                    ),
                  ),
                ),
                decoration: BoxDecoration(
                  color: HexColor("#4A69FF"),
                  borderRadius: BorderRadius.all(Radius.circular(10))
                ),
                padding: EdgeInsets.fromLTRB(10, 10, 10, 10),

              ),
              
              SettingsForm()

          ],)
          ),
      )
    );
  }
}

class SettingsForm extends StatefulWidget {
  SettingsForm({Key key}) : super(key: key);

  @override
  _SettingsFormState createState() => _SettingsFormState();
}

class _SettingsFormState extends State<SettingsForm> {

  String dropdownValue;
  int _currentRegularExpenses;
  int _currentInvestment;
  int _currentPersonalExpenses;


  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);

    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          UserData userData = snapshot.data;
            return Column(
              children: <Widget>[
                DropdownButton<String>(
                  value: dropdownValue ?? "${userData.regularExpenses}% | ${userData.investment}% | ${userData.personalExpenses}%",
                  icon: Icon(Icons.arrow_downward),
                  iconSize: 24,
                  elevation: 16,
                  style: TextStyle(color: HexColor("#8A9EAD")),
                  
                  underline: Container(
                    height: 2,
                    color: HexColor("#4A69FF"),

                  ),
                  onChanged: (String newValue) async{
                    setState(() {
                      dropdownValue = newValue;
                      String aux;
                      aux = newValue.substring(0, 2);
                      _currentRegularExpenses = int.parse(aux);
                      aux = newValue.substring(6, 8);
                      _currentInvestment = int.parse(aux);
                      aux = newValue.substring(12, 14);
                      _currentPersonalExpenses = int.parse(aux);
                    });
                    await DatabaseService(uid: user.uid).updateUserData(
                      snapshot.data.username, 
                      _currentRegularExpenses ?? snapshot.data.regularExpenses,
                      _currentInvestment ?? snapshot.data.investment, 
                      _currentPersonalExpenses ?? snapshot.data.personalExpenses,
                      snapshot.data.totalBudget,
                      snapshot.data.currentBudget,
                      _currentRegularExpenses * snapshot.data.totalBudget / 100 ?? snapshot.data.currentRegularExpenses,
                      _currentInvestment * snapshot.data.totalBudget / 100 ?? snapshot.data.currentInvestment,
                      _currentPersonalExpenses * snapshot.data.totalBudget / 100 ?? snapshot.data.currentPersonalExpenses
                    ); 

                  },
                  items: <String>['50% | 30% | 20%', '80% | 10% | 10%', '40% | 40% | 20%']
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                ),
                SizedBox(height:50),
              ],
            );
        }
        else
          return Loading();
      }
    );
  }
}

class BudgetInput extends StatefulWidget {
  @override
  _BudgetInputState createState() => _BudgetInputState();
}

class _BudgetInputState extends State<BudgetInput> {

  String error = "";
  double _totalBudget;

  @override
  Widget build(BuildContext context) {
    User user = Provider.of<User>(context);
    return StreamBuilder<UserData>(
      stream: DatabaseService(uid: user.uid).userData,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          UserData userData = snapshot.data;
          return SizedBox(
                height:44,
                child: TextFormField(
                  initialValue:  "${userData.totalBudget}",
                  errorTextPresent: false,
                  decoration: InputDecoration(  
                    hintText: 'Budget',
                    hintStyle:  TextStyle(
                          color: HexColor("#1A1A1A").withOpacity(0.6),
                          fontSize: 14,
                          height: 0.85,
                        ),
                    enabledBorder: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(40.0),
                    borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),
                    ),
                    
                    border: new OutlineInputBorder(
                    borderRadius: new BorderRadius.circular(40.0),
                    borderSide: BorderSide(color: HexColor("#1A1A1A").withOpacity(0.2)),

                    ),
                  ),
                  validator: (val) {
                      if (val.isEmpty){
                        setState(() => error += "Enter a budget\n");
                        return '';
                      }
                      else {
                        return null;
                      }
                    },

                  onChanged: (val) async {
                    setState(() => _totalBudget = double.parse(val));
                    await DatabaseService(uid: user.uid).updateUserData(
                      snapshot.data.username, 
                      snapshot.data.regularExpenses,
                      snapshot.data.investment, 
                      snapshot.data.personalExpenses,
                      _totalBudget ?? snapshot.data.totalBudget,
                      _totalBudget ?? snapshot.data.currentBudget,
                      _totalBudget * snapshot.data.regularExpenses / 100 ?? snapshot.data.currentRegularExpenses,
                      _totalBudget * snapshot.data.investment / 100 ?? snapshot.data.currentInvestment,
                      _totalBudget * snapshot.data.personalExpenses / 100 ?? snapshot.data.currentPersonalExpenses
                    ); 

                  },
                ),
              );

        }
        else
          return Loading();
      }

    );
  }
}